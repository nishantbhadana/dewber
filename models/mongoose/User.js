var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Promise = require("bluebird");

const maxAttempts = 5;
const lockoutHours = 30 * 60 * 1000; //30 minutes 
var UserSchema = new mongoose.Schema({
  account_id: {
    type: Number,
    unique: true,
    required: true,
  },
  first_name: {
    type: String
  },
  last_name: {
    type: String
  },
  email: {
    type: String,
    unique: true,
    index: true,
  },
  password: {
    type: String,
    expose: false
  },
  userType: {
    type: String,
    default: "USER"
  },
  login_attempts: {
    type: Number,
    required: true,
    default: 0
  },
  lock_until: Number,
  created_date: {
    type: Date,
    default: Date.now
  },
  last_login: {
    type: Date,
    default: Date.now
  },
  scholar_details: {
    is_scholar: String,
    year: Number,
    university: String,
    course: String,
    is_panel_mentor: String
  },
  personal_details: {
    phone_number: String,
    mobile_number: String,
    gender: String,
    dob: Date,
    preferredModeOfCommunication: String,
    communicationDetails: String,
  },
  address_details: {
    country: String,
    state: String,
    city: String,
    street: String,
    building: String,
    zipCode: String,
  },

  identity: {
    identity_doc_type: String,
    identity_file_name: String,
    address_doc_type: String,
    address_file_name: String
  },
  two_factor_secret: {
    type: String
  },
  two_factor_enabled: {
    type: Boolean,
    default: false
  },
  referral_id: {
    type: String,
    unique: true
  },
  reference_id: {
    type: String
  }
});
UserSchema.virtual('isLocked').get(function () {
  return !!(this.lock_until && this.lock_until > Date.now());
});

UserSchema.methods = {
  findByQuery: function (query, cb) {
    if (cb) return this.model('User').find(query).lean().exec(cb);
    return new Promise((resolve, reject) => {
      return mongoose.model('User').find(query).lean().exec().then((data) => {
        resolve(data);
      }).catch((e) => {
        reject(e);
      });
    });
  },
  removeUser: function (email) {
    let query = {
      email: email
    };
    return new Promise((resolve, reject) => {
      return mongoose.model('User')
        .findOne(query)
        .remove()
        .exec()
        .then((res) => {
          return resolve(res)
        })
        .catch((e) => {
          return reject(e);
        });
    });
  },
  updateById: function (email, data, cb) {
    var query = {
      email: email
    };
    if (cb) return this.model('User').update(query, data, cb);
    return new Promise((resolve, reject) => {
      return mongoose.model('User').update(query, data)
        .then((res) => {
          resolve(res);
        }).catch((e) => {
          reject(e);
        })
    });
  },
  updateByObjectId: function (id, data, cb) {
    id = mongoose.Types.ObjectId(id);
    var query = {
      _id: id
    }
    if (cb) return this.model('User').update(query, data, cb);
    return new Promise((resolve, reject) => {
      return mongoose.model('User').update(query, data)
        .then((res) => {
          resolve(res);
        })
        .catch((e) => {
          reject(res);
        });
    });
  },
  updateData: function (query, data, cb) {
    if (cb) return this.model('User').update(query, data, cb);
    return new Promise((resolve, reject) => {
      return mongoose.model('User').update(query, data)
        .then((res) => {
          resolve(res);
        }).catch((e) => {
          reject(e);
        })
    });
  },
  checkUsersExists: function (query, cb) {
    if (cb) return this.model('User').find({
      $or: query
    }).lean().exec(cb);
    return new Promise((resolve, reject) => {
      return mongoose.model('User').find({
        $or: query
      }).lean().exec()
        .then((res) => {
          resolve(res)
        })
        .catch((e) => {
          reject(e);
        });
    });
  },
  getByLastAccountId: function (cb) {
    return this.model('User').find({}).sort({
      'account_id': -1
    }).lean().limit(1).exec(cb);
  },
  getDataByPagination: function (query, sortQuery, page, limit) {
    return new Promise((resolve, reject) => {
      return mongoose.model('User')
        .find(query)
        .sort(sortQuery)
        .limit(limit)
        .skip(page * limit)
        .lean()
        .exec()
        .then((res) => {
          return resolve(res);
        })
        .catch((e) => {
          return reject(e);
        })
    });
  },
  getCountByQuery: function (query) {
    return new Promise((resolve, reject) => {
      return mongoose.model('User')
        .count(query)
        .then((res) => {
          return resolve(res);
        })
        .catch((e) => {
          return reject(e);
        })
    });
  },
  generateHash: function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  },
  validPassword: function (password) {
    if (typeof this.password != "undefined" && this.password.trim().length > 0)
      return bcrypt.compareSync(password, this.password);
    return false;
  },
  updateLastLoginTime: function (uniqueId, callback) {

    var data = {
      login_attempts: 0,
      last_login: new Date()
    }

    if (uniqueId) {
      data.api_key = uniqueId
    }
    var updates = {
      $set: data
    };
    return this.update(updates, callback);

  },
  incrementLoginAttempts: function (callback) {
    var lockExpired = !!(this.lock_until && this.lock_until < Date.now());
    console.log("lockExpired", lockExpired);
    if (lockExpired) {
      return this.update({
        $set: {
          login_attempts: 1
        },
        $unset: {
          lock_until: 1
        }
      }, callback);
    }
    var updates = {
      $inc: {
        login_attempts: 1
      }
    };
    var needToLock = !!(this.login_attempts + 1 >= maxAttempts && !this.isLocked);

    if (needToLock) {
      updates.$set = {
        lock_until: Date.now() + lockoutHours
      };
      console.log("config.login.lockoutHours", Date.now() + lockoutHours)
    }
    return this.update(updates, callback);
  }
}
// create the model for users and expose it to our app
module.exports = mongoose.model('User', UserSchema, 'ChevUser');