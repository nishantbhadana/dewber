$(document).ready(function() {
	$('.scroll-target').click(function(e) {
		var target = $(this).attr("data-target");
		console.log(target);
		if (typeof target !== "undefined") {
			$('html, body').animate({
				scrollTop: $(target).offset().top + 150
			}, 1000);
		}
	});
});