$('.navbar-toggle-icon li a').on('click', function () {
    $("body").toggleClass("aside-collapsed");

});

// $('.chosen-select').chosen({ width: "100%" });
// $('.summernote').summernote();
// $('#data_1 .input-group.date').datepicker({
//     todayBtn: "linked",
//     keyboardNavigation: false,
//     forceParse: false,
//     calendarWeeks: true,
//     autoclose: true
// });

// $(function () {
//     //Dropzone
//     Dropzone.options.myDropzone = {
//         paramName: "file",
//         maxFilesize: null,
//         addRemoveLinks: "true"
//     };
// });

$('.Users-list').click(function () {
    var value = $(this).children().eq(2).html();
    var trim = value.trim()
    var data = {
        "email": trim
    }
    document.location.href = 'verify-details?email=' + trim;
})

function validate() {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
}

$('#kyc-select-all').click(function (event) {
    if (this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function () {
            this.checked = true;
        });
    } else {
        $(':checkbox').each(function () {
            this.checked = false;
        });
    }
});