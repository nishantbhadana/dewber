var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var RedisStore = require('connect-redis')(session);

//Middleware
var authUser = require('./middleware/auth_user');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var dashboardRouter = require('./routes/dashboard');
var accountsRouter = require('./routes/accounts');
var referralsRouter = require('./routes/referrals');
var adminDashboardRouter = require('./routes/dashboard-admin');


var config = require('./config/config'); //Config File
var mongoose = require('./config/mongoose');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var redisConOptions = {
  host: config.getRedisHostName(),
  port: config.getRedisPort(),
  pass: config.getRedisPassword(),
}
app.use(session({
  store: new RedisStore(redisConOptions),
  secret: 'session_cookie_secret',
  logErrors: true,
  prefix: 'sess',
  saveUninitialized: false,
  resave: true,
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

require('./config/passport.js')(passport);

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/dashboard', authUser.isValidUser, dashboardRouter);
app.use('/accounts', authUser.isValidUser, accountsRouter);
app.use('/referrals', authUser.isValidUser, referralsRouter);
app.use("/admin", authUser.isValidUser, adminDashboardRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;