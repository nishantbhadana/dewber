/**
 * MongoDB Config Settings 
 */
const MONGODB_HOST_NAME = "localhost";
const MONGODB_PORT = "27017";
const MONGODB_DATABASE = "chevening";
const MONGODB_USER_NAME = "chevening_admin";
const MONGODB_USER_PASSWORD = "CheveningPassw00rd321";


/** 
 * Redis Config Settings
 */
const REDIS_HOST_NAME = "localhost";
const REDIS_PORT = 6379;
const REDIS_PASSWORD = "K3vnbPozHfiFRmxDAoLgSNGNZeK4xyit";

class Config {
  constructor() {

  }
  getMongoDBHostName() {
    return MONGODB_HOST_NAME;
  }
  getMongoDBPort() {
    return MONGODB_PORT;
  }
  getMongoDBDatabase() {
    return MONGODB_DATABASE;
  }
  getMongoDBUserName() {
    return MONGODB_USER_NAME;
  }
  getMongoDBPassword() {
    return MONGODB_USER_PASSWORD;
  }
  getRedisHostName() {
    return REDIS_HOST_NAME;
  }
  getRedisPort() {
    return REDIS_PORT;
  }
  getRedisPassword() {
    return REDIS_PASSWORD;
  }
}

var config = new Config();

module.exports = config;