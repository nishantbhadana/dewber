var mongoose = require('mongoose');
var config = require('../config/config')

// const uri = 'mongodb://' + config.getMongoDBUserName() + ':' + config.getMongoDBPassword() + '@' + config.getMongoDBHostName() + ':' + config.getMongoDBPort() + '/' + config.getMongoDBDatabase();

const uri = 'mongodb://' + config.getMongoDBHostName() + ':' + config.getMongoDBPort() + '/' + config.getMongoDBDatabase();

var options = {
  keepAlive: true,
  reconnectTries: Number.MAX_VALUE,
  useMongoClient: true
}

mongoose.Promise = require('bluebird');

mongoose.connect(uri, options, function (err, db) {
  if (err) console.log("MongoDB connection Err:: " + err);
  else console.log("------ Chevening MongoDB Connection OK ------- ");
});


exports.mongoose = mongoose;