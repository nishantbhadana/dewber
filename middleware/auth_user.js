module.exports = {
  isValidUser: (req, res, next) => {
    if (req.isAuthenticated()) {
      if (req.user.two_factor_enabled) {
        if (req.session.two_factor_verified) {
          res.locals.user = req.user;
          return next();
        } else {
          return res.redirect('/two_factor_verify');
        }
      } else {
        res.locals.user = req.user;
        return next();
      }
    }
    return res.redirect('/login');
  },
  updateSession: (req, data) => {
    return new Promise((resolve, reject) => {
      let userData = req.user;
      for (var key in data) userData[key] = data[key];
      req.login(userData, function (err) {
        if (err) reject(err);
        resolve(true);
      });
    });
  }
}