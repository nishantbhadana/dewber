var express = require('express');
var router = express.Router();
var validator = require("validator");
var bcrypt = require('bcrypt-nodejs');
const speakeasy = require("speakeasy");
const QRCode = require('qrcode');
const bluebird = require('bluebird');
bluebird.promisifyAll(QRCode);

const UserModel = require('../models/mongoose/User');
let userModel = new UserModel();

const defaultTitle = "Dewber - Accounts";

const generateOTPAuthUrl = (email, secretKey) => {
  return "otpauth://totp/Dewber.com@" + email + "?secret=" + secretKey + "&issuer=Dewber.com";
}

const updateSession = (req, data) => {
  return new Promise((resolve, reject) => {
    let userData = req.user;
    for (var key in data) userData[key] = data[key];
    req.login(userData, function (err) {
      if (err) reject(err);
      resolve(true);
    });
  });
}

router.get('/', async (req, res, next) => {
  let data = {};
  try {
    let userData = req.user;
    let userSecretKey = null;
    if (!userData.two_factor_secret) {
      var secret = speakeasy.generateSecret({
        length: 10
      });
      userSecretKey = secret.base32;
      let updateData = {
        two_factor_secret: userSecretKey
      };
      await userModel.updateById(userData.email, updateData);
      await updateSession(req, updateData);
    } else {
      userSecretKey = userData.two_factor_secret;
    }
    data['secret_key'] = userSecretKey;
    let otpauth_url = generateOTPAuthUrl(userData.email, userSecretKey);
    let dataImage = await QRCode.toDataURL(otpauth_url);
    data['qr_image'] = dataImage;
    data['title'] = defaultTitle;
  } catch (e) {
    console.log(e);
  }
  res.locals.user = req.user;
  return res.render('dashboard/accounts', data);
});


router.post('/update_profile', async (req, res, next) => {
  let response = {
    status: 500
  };
  try {
    var full_name = req.body.full_name;
    var email = req.body.email;
    let sessionEmail = req.user.email;
    if (full_name && validator.isEmail(email)) {
      let first_name = full_name.split(" ")[0];
      let last_name = full_name.split(" ")[1];
      userModel = new UserModel();
      if (sessionEmail == email) {
        let data = {
          first_name: first_name,
          last_name: last_name
        };
        await userModel.updateById(email, data);
        response.status = 200;
        response.msg = "Successfully updated";
      } else {
        var query = {
          email: email
        };
        var docs = await userModel.findByQuery(query);
        if (docs.length <= 0) {
          updateData = {
            email: email
          }
          query = {
            email: sessionEmail
          }
          await userModel.updateData(query, updateData);
          response.status = 200;
          response.msg = "Successfully updated";
        } else {
          response.msg = "Email Already Exists";
        }
      }
    } else {
      response.msg = "Params required";
    }



  } catch (e) {
    console.log(e);
  }
  return res.json(response);
});

router.post('/update_password', async (req, res, next) => {
  var password = req.body.password;
  var new_password = req.body.new_password;
  var response = {
    status: 400,
    msg: "Invalid Password"
  }
  try {
    var query = {
      email: req.user.email
    }
    var docs = await userModel.findByQuery(query);
    if (docs.length > 0) {
      var dbPassword = docs[0].password;
      if (bcrypt.compareSync(password, dbPassword)) {
        var hashNewPassword = userModel.generateHash(new_password);
        var updateData = {
          password: hashNewPassword
        }
        await userModel.updateData(query, updateData);
        response.status = 200;
        response.msg = "Your password has been successfully updated";
      }
    }
  } catch (e) {
    console.log(e);
  }
  return res.json(response);
});

router.post('/enable_ga', async (req, res, next) => {
  let response = {
    status: 400,
    msg: "Invalid User/Password"
  };
  try {
    let password = req.body.password;
    let token = req.body.ga_code;
    let email = req.user.email;
    var query = {
      email: email
    }
    var docs = await userModel.findByQuery(query);
    if (docs.length > 0) {
      var dbPassword = docs[0].password;
      if (bcrypt.compareSync(password, dbPassword)) {
        let userSecretKey = req.user.two_factor_secret;
        let verified = speakeasy.totp.verify({
          secret: userSecretKey,
          encoding: 'base32',
          token: token,
          window: 12
        });
        if (verified) {
          let updateData = {
            two_factor_enabled: true
          };
          await userModel.updateById(email, updateData);
          await updateSession(req, updateData);
          response.status = 200;
          response.msg = "Success";
        } else {
          response.msg = "Invalid Token";
        }
      }
    }
  } catch (e) {
    console.log(e);
  }
  return res.json(response);
});

router.post('/disable_ga', async (req, res, next) => {
  let response = {
    status: 400,
    msg: "Invalid option"
  };
  try {
    let email = req.user.email;
    let updateData = {
      two_factor_enabled: false
    };
    await userModel.updateById(email, updateData);
    response.status = 200;
    response.msg = "Success";
  } catch (e) {
    console.log(e);
    response.exception = e.toString();
  }
  return res.json(response);
});


module.exports = router;