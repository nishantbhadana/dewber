var express = require('express');
var router = express.Router();

const UserSchema = require('../models/mongoose/User');

const isValidUser = (req, res, next) => {
    console.log(req.isAuthenticated());
    console.log(req.user);
    // if (req.isAuthenticated()) return next();
    // else return res.redirect('/login');
}

router.get('/dashboard', async (req, res, next) => {
    let data = {}
    let usersList = []
    UserSchema.find({ userType: "USER" }, { password: 0 }).then((user) => {
        usersList = user
    }).then(() => {
        data['list'] = usersList
        return res.render('dashboardAdmin/index', data);
    }).error((error) => {
        console.log(error);
    })
});

router.get('/verify-details', async (req, res, next) => {
    let data = {};
    UserSchema.findOne({ email: req.query.email }, { password: 0 }).then((user) => {
        data['details'] = user
    }).then(() => {
        res.render('dashboardAdmin/verifyDocuments', data)
    }).error((error) => {
        console.log(error);
    })

});


router.get('/bonus-management', async (req, res, next) => {
    // let data = {};
    // UserSchema.findOne({ email: req.query.email }, { password: 0 }).then((user) => {
    //     data['details'] = user
    // }).then(() => {
    //     res.render('dashboardAdmin/verifyDocumnets', data)
    // }).error((error) => {
    //     console.log(error);
    // })
    res.render('dashboardAdmin/bonusManagement');
});

router.get('/referral-management', async (req, res, next) => {
    res.render('dashboardAdmin/referralManagement');
});

router.get('/cryptocurreny-management', async (req, res, next) => {
    res.render('dashboardAdmin/cryptocurrenyManagement');
});
router.get('/investor-master', async (req, res, next) => {
    res.render('dashboardAdmin/investorMaster');
});
router.get('/admin-user-management', async (req, res, next) => {
    res.render('dashboardAdmin/adminUserManagement');
});

router.get('/dashboard-settings', async (req, res, next) => {
    res.render('dashboardAdmin/dashboardSettings');
})

router.get('/kyc-management', async (req, res, next) => {
    let data = {}
    let usersList = []
    UserSchema.find({ userType: "USER" }, { password: 0 }).then((user) => {
        usersList = user
    }).then(() => {
        data['list'] = usersList
        return res.render('dashboardAdmin/kycManagement', data);
    }).error((error) => {
        console.log(error);
    })
});

module.exports = router;