var express = require('express');
var router = express.Router();
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // cb(null, '/var/www/Archive/chevening/public/uploads/')
    cb(null, './public/uploads')
  },
  filename: function (req, file, cb) {
    let extension = file.originalname.split(".");
    extension = extension[extension.length - 1];
    cb(null, file.fieldname + '-' + Date.now() + "." + extension)
  }
})

const upload = multer({
  storage: storage
})

const UserModel = require('../models/mongoose/User');
let userModel = new UserModel();

const defaultTitle = "Dewber - Dashboard";

const isValidUser = (req, res, next) => {
  console.log(req.isAuthenticated());
  console.log(req.user);
  // if (req.isAuthenticated()) return next();
  // else return res.redirect('/login');
}
router.get('/', async (req, res, next) => {

  let data = {};
  try {
    data['title'] = defaultTitle;
  } catch (e) {
    console.log(e);
  }
  res.locals.user = req.user;
  console.log(req.user);
  return res.render('dashboard/index', data);
  //} else {
  //  return res.redirect('/login');
  //}
});

router.post('/scholar_details', async (req, res, next) => {
  let response = {
    status: 500
  };
  try {
    let email = req.user.email;
    let is_scholar = req.body.scholar_group;
    let year = (req.body.year) ? parseInt(req.body.year) : 0;
    let university = (req.body.university) ? req.body.university : null;
    let course = (req.body.course) ? req.body.course : null;
    let mentors_group = (req.body.mentors_group) ? req.body.mentors_group : null;
    let data = {
      $set: {
        "scholar_details.is_scholar": is_scholar,
        "scholar_details.year": year,
        "scholar_details.university": university,
        "scholar_details.course": course,
        "scholar_details.is_panel_mentor": mentors_group
      }
    };
    await userModel.updateById(email, data);
    response.status = 200;
    response.msg = "Success";
  } catch (e) {
    console.log(e);
  }
  return res.json(response);
});

router.post('/personal_details', async (req, res, next) => {
  let response = {
    status: 500
  };

  console.log('===============personal-details=====================');
  console.log(req.user);
  console.log('===============personal-details=====================');

  try {
    let email = req.user.email;
    let first_name = req.body.first_name;
    let last_name = req.body.last_name;
    let gender = req.body.gender;
    let dob = req.body.dob;
    let mobile_number = req.body.mobile_number;
    let phone_number = req.body.phone_number;
    let country = req.body.country;
    let state = req.body.state;
    let city = req.body.city;
    let building = req.body.building;
    let zipCode = req.body.zipCode;
    let communicationDetails = req.body.communicationDetails;
    let preferredModeOfCommunication = req.body.preferredModeOfCommunication;
    let street = req.body.street;
    let data = {
      $set: {
        "first_name": first_name,
        "last_name": last_name,
        "personal_details.phone_number": phone_number,
        "personal_details.gender": gender,
        "personal_details.dob": dob,
        "personal_details.communicationDetails": communicationDetails,
        "personal_details.mobile_number": mobile_number,
        "personal_details.preferredModeOfCommunication": preferredModeOfCommunication,
        "address_details.country": country,
        "address_details.state": state,
        "address_details.city": city,
        "address_details.street": street,
        "address_details.building": building,
        "address_details.zipCode": zipCode,
      }
    };
    await userModel.updateById(email, data);
    response.status = 200;
    response.msg = "Success";
  } catch (e) {
    console.log(e);
  }
  return res.json(response);
});

const uploadFields = upload.fields([{
  name: 'identity_document',
  maxCount: 1
}, {
  name: 'address_document',
  maxCount: 1
}]);

router.post('/identity', uploadFields, async (req, res, next) => {
  let response = {
    status: 500
  };
  try {
    let email = req.user.email;
    let identity_doc_type = req.body.identity_doc_type;
    let identity_file_name = req.files.identity_document[0].filename;
    let address_doc_type = req.body.address_doc_type;
    let address_file_name = req.files.address_document[0].filename;
    let data = {
      $set: {
        "identity": {
          identity_doc_type: identity_doc_type,
          identity_file_name: identity_file_name,
          address_doc_type: address_doc_type,
          address_file_name: address_file_name
        }
      }
    };
    await userModel.updateById(email, data);
    response.status = 200;
    response.msg = "Success";
  } catch (e) {
    console.log(e);
  }
  return res.json(response);
});


module.exports = router;