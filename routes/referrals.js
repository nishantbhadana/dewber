var express = require('express');
var router = express.Router();
const generatePassword = require('password-generator');
//Modals 
const UserModel = require('../models/mongoose/User');
//Middleware
var authUser = require('../middleware/auth_user');
let userModel = new UserModel();
require('../config/constants');

router.get('/', async (req, res, next) => {
  let data = {};
  try {
    data['title'] = DEFAULT_SITE_TITLE;
    let referralId = null;
    if (req.user.referral_id) {
      referralId = req.user.referral_id;
    } else {
      referralId = generatePassword();
      let updateDate = {
        referral_id: referralId
      };
      await userModel.updateById(req.user.email, updateDate);
      await authUser.updateSession(req, updateDate);
    }
    let query = {
      reference_id: referralId
    };
    let usersCount = await userModel.getCountByQuery(query);
    data['users_count'] = (isNaN(usersCount)) ? 0 : usersCount;
    data['referral_url'] = DOMAIN_URL + "?ref=" + req.user.referral_id;
  } catch (e) {
    console.log(e);
  }
  return res.render('dashboard/referrals', data);
});


module.exports = router