var express = require('express')
var router = express.Router()
var passport = require('passport')
var Recaptcha = require('express-recaptcha').Recaptcha;
const speakeasy = require("speakeasy");

require('../config/constants')
var recaptcha = new Recaptcha(SITE_KEY, SECRET_KEY)

const defaultTitle = 'Chevening Fund - A Fund for Social Impact';

const isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) return next()
  return res.redirect('/login');
}
const isTwoFactorAuthenticated = (req) => {
  if (req.isAuthenticated()) {
    if (req.user.two_factor_enabled) {
      if (req.session.two_factor_verified) return true;
    } else return true;
  }
  return false;
}

/* GET home page. */
router.get('/', async (req, res, next) => {
  let data = {}
  try {
    data['title'] = defaultTitle;
    let reference_id = req.query.ref;
    if (reference_id) {
      let options = {
        maxAge: 1000 * 60 * 60 * 90, //3 months
        httpOnly: true
      }
      res.cookie('reference_id', reference_id, options)
    }
  } catch (e) {
    console.log(e)
  }
  return res.render('home/index', data)
})

/**
 * GET about page.
 */
router.get('/about', async (req, res, next) => {
  let data = {}
  try {
    data['title'] = defaultTitle
  } catch (e) {
    console.log(e)
  }
  return res.render('home/about', data)
})

/**
 * GET login page.
 */
router.get('/login', recaptcha.middleware.render, async (req, res, next) => {
  if (isTwoFactorAuthenticated(req)) return res.redirect('/dashboard')
  let data = {}
  try {
    data['title'] = defaultTitle
  } catch (e) {
    console.log(e)
  }
  data['message'] = req.flash('loginMessage')
  data['captcha'] = res.recaptcha
  return res.render('home/login', data)
})

/**
 * POST login
 */
router.post(
  '/login',
  recaptcha.middleware.verify,
  passport.authenticate('local-login', {
    failureRedirect: '/login'
  }),
  (req, res) => {
    if (req.isAuthenticated()) {
      if (req.user.two_factor_enabled) {
        return res.redirect('/two_factor_verify')
      } else {
        if (req.user.userType === "ADMIN") {
          return res.redirect('/admin/dashboard');
        } else {
          return res.redirect('/dashboard');
        }
        // return res.redirect('/dashboard')
      }
    } else {
      return res.redirect('/login')
    }
  }
)

router.get('/two_factor_verify', isAuthenticated, async (req, res, next) => {
  if (isTwoFactorAuthenticated(req)) return res.redirect('/dashboard')
  let data = {}
  try {
    data['title'] = defaultTitle;
    data['message'] = []
  } catch (e) {
    console.log(e)
  }
  return res.render('home/two_factor_verify', data)
})

router.post('/two_factor_verify', isAuthenticated, async (req, res, next) => {
  let data = {};
  data['title'] = defaultTitle
  try {
    let token = req.body.token;
    if (token) {
      let userSecretKey = req.user.two_factor_secret;
      let verified = speakeasy.totp.verify({
        secret: userSecretKey,
        encoding: 'base32',
        token: token,
        window: 12
      });
      if (verified) {
        req.session.two_factor_verified = true;
        return res.redirect('/dashboard');
      }
    }

  } catch (e) {
    console.log(e);
  }
  data['message'] = ['Invalid Token'];
  return res.render('home/two_factor_verify', data);
})

/**
 * GET signup page.
 */
router.get('/register', recaptcha.middleware.render, async (req, res, next) => {
  if (isTwoFactorAuthenticated(req)) return res.redirect('/dashboard')
  let data = {}
  try {
    data['title'] = defaultTitle
  } catch (e) {
    console.log(e)
  }
  data['captcha'] = res.recaptcha
  data['message'] = req.flash('loginMessage')
  console.log(data['message'])
  // console.log(req.flash('loginMessage'));
  return res.render('home/signup', data)
})

/**
 * POST Signup
 */
router.post(
  '/signup',
  recaptcha.middleware.verify,
  passport.authenticate('local-signup', {
    failureRedirect: '/register', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
  }),
  (req, res) => {
    if (req.isAuthenticated()) {
      if (req.user) {
        return res.redirect('/dashboard')
      } else {
        return res.redirect('/login')
      }
    } else {
      return res.redirect('/register')
    }
  }
)

router.get('/logout', async (req, res, next) => {
  req.session.destroy()
  req.logout()
  res.redirect('/')
})

module.exports = router